package com.yeamgood.letterword;

import java.util.ArrayList;

public abstract class LetterWord {

    public ArrayList<String> letterWordList = new ArrayList<>();

    public LetterWord() {
        initialLetterWordList();
    }

    public abstract void initialLetterWordList();

    public ArrayList<String> getLetterWordList(){
        return this.letterWordList;
    }
}
