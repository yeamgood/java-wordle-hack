package com.yeamgood;

import com.yeamgood.alphabet.Alphabet;
import com.yeamgood.alphabet.EnglishAlphabet;
import com.yeamgood.letterword.English5LetterWord;
import com.yeamgood.letterword.LetterWord;
import com.yeamgood.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class WordleHackMain {

    private static final int maxRound = 6;

    public static void main(String[] args) throws IOException {
        boolean startGame = true;

        logoGame();
        while (startGame) {
            printStateMain();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String selectFunction = reader.readLine();
            System.out.println("you select function is " + selectFunction);

            if(selectFunction.equals("1")){
                playGame();
            }else if(selectFunction.equals("4")){
               printHowToPlay();
            }else if(selectFunction.equals("0")){
                startGame = false;
            }
        }
    }

    // #####################
    // ##### FUNCTION ######
    // #####################
    public static String randomWord(ArrayList<String> letterWordList){
        Random random = new Random();
        return letterWordList.get(random.nextInt(letterWordList.size()));
    }

    public static ArrayList<String> generateWordList(ArrayList<Word> alphabetList,ArrayList<String> logicWordList){
        int indexChar = 0;
        ArrayList<String> newLetterWordTemp;

        // First Round check Correct
        for (Word letter:alphabetList) {
            if(letter.isCorrectSpot()){
                newLetterWordTemp = new ArrayList<>();
                indexChar = letter.getCorrectSpotIndex();
                for (String word:logicWordList){
                    if(word.substring(indexChar,indexChar+1).equals(letter.getWord())){
                        newLetterWordTemp.add(word);
                    }
                }
                logicWordList = newLetterWordTemp;
            }
        }

        // Second Round check wrong
        for (Word letter:alphabetList) {
            if(letter.isWrongSpot()){
                //System.out.println("isWrongSpot");
                newLetterWordTemp = new ArrayList<>();
                indexChar = letter.getWrongSpotIndex();
                for (String word:logicWordList){
                    if(word.contains(letter.getWord())){
                        if(!word.substring(indexChar,indexChar+1).equals(letter.getWord())){
                            newLetterWordTemp.add(word);
                        }
                    }
                }
                logicWordList = newLetterWordTemp;
            }
        }

        // third Round check wrong
        for (Word letter:alphabetList) {
            if(letter.isNotFound()){
//                System.out.println("isNotFound");
                newLetterWordTemp = new ArrayList<>();
                for (String word:logicWordList){
                    if(!word.contains(letter.getWord())){
                        newLetterWordTemp.add(word);
                    }
                }
                logicWordList = newLetterWordTemp;
            }
        }
        return logicWordList;
    }


    public static void initialAlphabet(ArrayList<Word> wordles){
        if(wordles == null){
            wordles = new ArrayList<>();
        }
        Alphabet alphabetList = new EnglishAlphabet();
        for(String  alphabet: alphabetList.getAlphabetList()){
            wordles.add(new Word(alphabet));
        }
    }

    public  static void printAlphabet(ArrayList<Word> wordles){
        //System.out.println("Letter Word.");
        if(wordles != null){
             for (Word wordle:wordles){
                 System.out.print(wordle.getWordColor()  + " ");
             }
        }
        System.out.println();
    }

    public static void printTextBox(int row,int column,ArrayList<WordList> userWordList){
        String charRow = "------";
        String charColumn = "|     ";
        boolean isOneTimeWriteTop = false;
        for (int rowIndex = 0; rowIndex < row; rowIndex++) {
            // write top row
            if(!isOneTimeWriteTop) {
                for (int columnIndex = 0; columnIndex < column; columnIndex++) {
                    System.out.print(charRow);
                }
                System.out.println();
                isOneTimeWriteTop = true;
            }

            //write column
            for (int columnIndex = 0; columnIndex <= column; columnIndex++) {
                if(rowIndex < userWordList.size()){
                    ArrayList<Word> wordlestemp = userWordList.get(rowIndex).getWordList();
                   if(columnIndex < wordlestemp.size()){
                       System.out.print("|  " + wordlestemp.get(columnIndex).getWordColor() + "  ");
                   }else{
                       System.out.print(charColumn);
                   }
                }else{
                    System.out.print(charColumn);
                }
            }
            System.out.println();

            // write bottom row
            for (int columnIndex = 0; columnIndex < column; columnIndex++) {
                System.out.print(charRow);
            }
            System.out.println();
        }
    }

    public static void convertWordToWordList(String word, WordList wordleList,ArrayList<Word> alphabetList){
        if(wordleList == null) {
            wordleList = new WordList();
        }

        for (int i = 0; i < word.length(); i++) {
            for (Word letter:alphabetList) {
                if(word.substring(i,i+1).equals(letter.getWord())){
                    wordleList.getWordList().add(letter);
                }
            }
        }

    }

    public static void convertWordToWordList(String word,WordList wordList){
        if(wordList == null) {
            wordList = new WordList();
        }

        for (int i = 0; i < word.length(); i++) {
            wordList.getWordList().add(new Word(word.substring(i,i+1)));
        }
    }

    private static boolean checkCorrectWord(ArrayList<Word> hintWord, ArrayList<Word> userWord) {
        boolean isAllCorrect = true;
        Word wordTemp;
        for (int i = 0; i < userWord.size(); i++) {
            for (int j = 0; j < hintWord.size(); j++) {
                if (userWord.get(i).getWord().equals(hintWord.get(j).getWord())) {
                    if (i == j) {
                        userWord.get(i).setCorrectSpot(true);
                        userWord.get(i).setWrongSpot(false);
                        userWord.get(i).setNotFound(false);
                        userWord.get(i).setCorrectSpotIndex(i);
                        break;
                    } else {
                        userWord.get(i).setWrongSpot(true);
                        userWord.get(i).setNotFound(false);
                        userWord.get(i).setWrongSpotIndex(i);
                    }
                }
                if (userWord.get(i).isCorrectSpot() || userWord.get(i).isWrongSpot()) {
                    userWord.get(i).setNotFound(false);
                } else {
                    userWord.get(i).setNotFound(true);
                }
            }
        }

        for (int i = 0; i < userWord.size(); i++) {
            //System.out.println(userWord.get(i).getWord() + " vs " + hintWord.get(i).getWord());
            if (!userWord.get(i).getWord().equals(hintWord.get(i).getWord())) {
                isAllCorrect = false;
            }
        }
        return isAllCorrect;
    }

    private static void manageUserWord(ArrayList<WordList> userWordList, WordList wordleList){
        Word word;
        WordList wordListTemp = new WordList();
        for (int i = 0; i < wordleList.getWordList().size(); i++) {
            word = new Word(wordleList.getWordList().get(i));
            if(word.isCorrectSpot() && word.getCorrectSpotIndex() != i){
                word.setCorrectSpot(false);
                word.setCorrectSpotIndex(null);
                word.setWrongSpot(false);
                word.setWrongSpotIndex(null);
                word.setNotFound(true);
            }
            wordListTemp.getWordList().add(word);
        }
        userWordList.add(wordListTemp);
    }

    private static void logoGame(){
         System.out.println(" .--------------. .--------------. .--------------. .--------------. .--------------. .--------------. ");
         System.out.println(" | _____  _____ | |     ____     | |  _______     | |  ________    | |   _____      | |  _________   | ");
         System.out.println(" ||_   _||_   _|| |   .'    `.   | | |_   __ \\    | | |_   ___ `.  | |  |_   _|     | | |_   ___  |  | ");
         System.out.println(" |  | | /`.| |  | |  /  .--.  \\  | |   | |__) |   | |   | |   `. \\ | |    | |       | |   | |_  \\_|  | ");
         System.out.println(" |  | |/  \\| |  | |  | |    | |  | |   |  __ /    | |   | |    | | | |    | |   _   | |   |  _|  _   | ");
         System.out.println(" |  |   /`.  |  | |  \\  `--'  /  | |  _| |  \\ \\_  | |  _| |___.' / | |   _| |__/ |  | |  _| |___/ |  | ");
         System.out.println(" |  |__/  \\__|  | |   `.____.'   | | |____| |___| | | |________.'  | |  |________|  | | |_________|  | ");
         System.out.println(" |              | |              | |              | |              | |              | |              | ");
         System.out.println(" '--------------' '--------------' '--------------' '--------------' '--------------' '--------------' ");
        System.out.println("  :: Wordle Game ::                                                                  (v.1.0.1.RELEASE)");
    }

    private static void miniLogo(){
        System.out.println(" _ _  _ _  _ _  _ _  _ _  _ _");
        System.out.println("|   ||   ||   ||   ||   ||   |");
        System.out.println("| W || O || R || D || L || E |");
        System.out.println("|_ _||_ _||_ _||_ _||_ _||_ _|");
    }

    private static void printStateMain(){
        System.out.println(" ");
        System.out.println("Main function game");
        System.out.println("1 : PLAY GAME");
        System.out.println("2 : SIMULATION");
        System.out.println("3 : ANALYSIS");
        System.out.println("4 : HOW TO PLAY");
        System.out.println("0 : EXIT");
        System.out.println("Please input number select?");
    }

    private static void printHowToPlay(){
        System.out.println("Wordle");
        System.out.println("");
        System.out.println("HOW TO PLAY");
        System.out.println("Guess the WORDLE in six tries.");
        System.out.println("Each guess must be a valid five-letter word. Hit the enter button to submit.");
        System.out.println("After each guess, the color of the tiles will change to show how close your guess was to the word.");
        System.out.println("");
        System.out.println("Examples");
        System.out.println("The letter W is in the word and in the correct spot.");
        System.out.println("");
        System.out.println("");
        System.out.println("The letter I is in the word but in the wrong spot.");
        System.out.println("");
        System.out.println("The letter U is not in the word in any spot.");
        System.out.println("");
        System.out.println("A new WORDLE will be available each day!");
    }

    private static void playGame() throws IOException {
        miniLogo();

        //INIT KEY
        LetterWord letterWordList = new English5LetterWord();

        LetterWord posibleLetterWordList = new English5LetterWord();

        ArrayList<String> wordList = letterWordList.getLetterWordList();
        ArrayList<String> wordLibraryList = posibleLetterWordList.getLetterWordList();
        String randomWordRound = "";
        WordList wordleList;

        // FIRST RANDOM KEYWORD
        String hintWord = randomWord(wordLibraryList);
        WordList hintWordList = new WordList();
        convertWordToWordList(hintWord,hintWordList);

        // INITIAL WORD GAME
        ArrayList<WordList> userWordList = new ArrayList<>();
        ArrayList<Word> alphabetList = new ArrayList<>();
        initialAlphabet(alphabetList);
        printTextBox(6,5    ,userWordList);
        printAlphabet(alphabetList);


        // ####################
        // ###    ROUND    ####
        // ####################
        //init first word
        boolean isAllCorrect = false;
        for (int i = 1; i <= 6; i++) {
            randomWordRound = inputWordByUser(wordList);

            //init List word
            wordleList = new WordList();
            convertWordToWordList(randomWordRound,wordleList,alphabetList);
            isAllCorrect = checkCorrectWord(hintWordList.getWordList(),wordleList.getWordList());

            manageUserWord(userWordList,wordleList);
            printTextBox(6,5    ,userWordList);
            printAlphabet(alphabetList);
            wordLibraryList = generateWordList(alphabetList,wordLibraryList);

            
            if(isAllCorrect){
                System.out.println("!!! Congratulations You Win !!!");
                break;
            }
        }
        if(!isAllCorrect){
            System.out.println("!!! You Lose !!!" + " Hint word is \"" + hintWord +  "\"");
        }
    }

    private static String inputWordByUser( ArrayList<String> wordLibraryList) throws IOException {
        String wordUser = "";
        while (true) {
            System.out.println("Input word?");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            wordUser = reader.readLine().toUpperCase();

            if (wordLibraryList.contains(wordUser)) {
                break;
            } else {
              //  System.out.println("This word \"" + wordUser + " is not found!");
                System.out.println("Not in word list");
            }
        }
        return wordUser;
    }
}
