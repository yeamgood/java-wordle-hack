package com.yeamgood.model;

import java.util.ArrayList;

public class WordList {
    private ArrayList<Word> wordList = new ArrayList<>();

    public WordList() {
    }

    public ArrayList<Word> getWordList() {
        return wordList;
    }

    public void setWordList(ArrayList<Word> wordList) {
        this.wordList = wordList;
    }
}
