package com.yeamgood.model;

public class Word {

    private String word;
    private boolean notFound;
    private boolean wrongSpot;
    private boolean correctSpot;
    private Integer correctSpotIndex;
    private Integer wrongSpotIndex;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public Word(String word) {
        this.word = word;
    }

    public Word(Word word) {
        this.word = word.word;
        this.notFound = word.notFound;
        this.wrongSpot = word.wrongSpot;
        this.correctSpot = word.correctSpot;
        this.correctSpotIndex = word.correctSpotIndex;
        this.wrongSpotIndex = word.wrongSpotIndex;
    }

    public Word(String word, boolean notFound, boolean wrongSpot, boolean correctSpot, Integer correctSpotIndex, Integer wrongSpotIndex) {
        this.word = word;
        this.notFound = notFound;
        this.wrongSpot = wrongSpot;
        this.correctSpot = correctSpot;
        this.correctSpotIndex = correctSpotIndex;
        this.wrongSpotIndex = wrongSpotIndex;
    }

    public String getWord() {
        return word;
    }

    public String getWordColor() {
        if(this.correctSpot){
            return ANSI_GREEN + word + ANSI_RESET;
        }else if(this.wrongSpot){
            return ANSI_YELLOW + word + ANSI_RESET;
        }else if(this.notFound){
            return ANSI_RED + word + ANSI_RESET;
        }
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public boolean isNotFound() {
        return notFound;
    }

    public void setNotFound(boolean notFound) {
        this.notFound = notFound;
    }

    public boolean isWrongSpot() {
        return wrongSpot;
    }

    public void setWrongSpot(boolean wrongSpot) {
        this.wrongSpot = wrongSpot;
    }

    public boolean isCorrectSpot() {
        return correctSpot;
    }

    public void setCorrectSpot(boolean correctSpot) {
        this.correctSpot = correctSpot;
    }

    public Integer getCorrectSpotIndex() {
        return correctSpotIndex;
    }

    public void setCorrectSpotIndex(Integer correctSpotIndex) {
        this.correctSpotIndex = correctSpotIndex;
    }

    public Integer getWrongSpotIndex() {
        return wrongSpotIndex;
    }

    public void setWrongSpotIndex(Integer wrongSpotIndex) {
        this.wrongSpotIndex = wrongSpotIndex;
    }

    @Override
    public String toString() {
        return "Word{" +
                "word='" + word + '\'' +
                ", notFound=" + notFound +
                ", wrongSpot=" + wrongSpot +
                ", correctSpot=" + correctSpot +
                ", correctSpotIndex=" + correctSpotIndex +
                ", wrongSpotIndex=" + wrongSpotIndex +
                '}';
    }
}
