package com.yeamgood.alphabet;

import java.util.ArrayList;

public abstract class Alphabet {

    public ArrayList<String> alphabetList = new ArrayList<>();

    public Alphabet() {
        initialAlphabetList();
    }

    public abstract void initialAlphabetList();

    public ArrayList<String> getAlphabetList(){
        return this.alphabetList;
    }

}
